This repo represents the server side of the mobile ad tracking project written in nodeJs with mongodb as the database.

To test your mobile app with the apis in this code do the following:-
* Make sure you have nodejs and mongodb installed in your system.
* clone this repo
* Open your terminal and cd into the root directory of this repo and enter "npm install". THis will install all dependencies needed in your system. 
* Once you have all dependencies, from the same root, run "npm start" and your nodeserver will start by default at port 3000.
* Now you can hit relevant api endpoints. It supports JSON for requests and responses